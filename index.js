const { adapters } = require('./src/adapters');

const main = async ()=>{
    try {
        const result = await adapters({id:2});
        if(result.statusCode !== 200) throw(result.message)
        console.log("Tu data es: ",result.data);    
        
    } catch (error) {
        console.log(error)
    }
}

main();

