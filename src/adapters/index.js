const { service } =require('../services');

const adapters = async ({id})=>{
    try {
        let {statusCode, data, message} = await service({id});
        // user = {personal, preference}
        return { statusCode, data, message}
    } catch (error) {
        console.log({step: 'adapter Adapters', error: error.toString()})
        return { statusCode: 500, message: error.toString()}
    }
    // return service();
}

module.exports = {
    adapters,
}