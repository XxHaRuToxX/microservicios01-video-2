const { FindUser, ExistUser} = require('../controllers')

const service = async ({id})=>{
    try {

        /* let { statusCode, data, message } = FindUser({id}) */
        const existUser = await ExistUser({id})
        if(existUser.statusCode !== 200) throw (existUser.message)
        if(!existUser.data) throw("No existe el usuario")
        let findUser = await FindUser({id})
        if(findUser.statusCode !== 200 ) throw(findUser.message);
        if(findUser.data.info.edad > 18){
            console.log("Usted es mayor de edad!");
        }
        return { statusCode: 200, data: findUser.data }
        
    } catch (error) {
        console.log({step: 'service Service', error: error.toString()})
        return { statusCode: 500, message: error.toString()}
    }

}
module.exports = {
    service,
}